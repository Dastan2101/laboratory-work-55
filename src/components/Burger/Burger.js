import React from 'react'
import Ingredients from "./Ingredients"

const Burger = (props) => {
    return (
        <div className="Burger">
            <div className="BreadTop">
                <div className="Seeds1">

                </div>
                <div className="Seeds2">

                </div>
            </div>
            {props.ingredients.map((ingredient, index) => {
                return (
                    <Ingredients key={index} name={ingredient.name} count={ingredient.count}/>
                )
            })}
            <div className="BreadBottom">
            </div>
            <div className="total-price-block">
                <span className="total-price">Price: {props.total}</span>
            </div>
        </div>
    )
};

export default Burger