import React, {Fragment} from 'react'

const Ingredients = (props) => {

    const getIngredientsName = () => {
        const names = [];
        for(let i = 0; i < props.count; i++) {
            names.push(props.name)
        }
        return names
    };


    return (
        <Fragment>
            {getIngredientsName().map((ingredientName, index) => {
                return (
                    <div className={`${ingredientName}`} key={index}>

                    </div>
                )
            })}
        </Fragment>
    )
};

export default Ingredients