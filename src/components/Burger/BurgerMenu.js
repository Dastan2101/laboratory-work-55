import React from 'react';
import "./Style.css"
const BurgerMenu = (props) => {

    return (
        props.ingredients.map((fill, key) => {

            return (
                <div className="add-ingredient" key={key}>
                    <img src={fill.image} alt="#" className="img" onClick={() => props.onClickAdd(fill.name)}/>
                    <p className="ing-name">{fill.name}</p>
                    <span className="count">x{fill.count}</span>

                    <span className="price">{fill.price} som</span>

                    {fill.show ? <button className="delete-btn" onClick={() => props.remove(fill.name)} style={{display: 'inline-block'}}>

                    </button> : null}

                </div>
            )

        })

    )
};

export default BurgerMenu;