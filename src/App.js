import React, {Component} from 'react';
import './App.css';
import meatImage from './components/assets/meat.jpg';
import cheeseImage from './components/assets/cheese.png';
import baconImage from './components/assets/becon.jpg';
import saladImage from './components/assets/salad.jpg';
import BurgerMenu from './components/Burger/BurgerMenu';
import Burger from  './components/Burger/Burger'


class App extends Component {
    state = {
        ingredients: [
            {name: 'Meat', count: 0, price: 50,image: meatImage, show: false},
            {name: 'Cheese', count: 0, price: 20,image: cheeseImage,show: false},
            {name: 'Salad', count: 0, price: 5,image: saladImage,show: false},
            {name: 'Bacon', count: 0, price: 30,image: baconImage,show: false},
        ],
        total: 20,
    };


    addIngredient = (name) => {


        const ingredients = [...this.state.ingredients];

        let copyTotal = this.state.total;

        for(let i = 0; i < ingredients.length; i++) {
            if(ingredients[i].name === name) {
                ingredients[i].count++;

                copyTotal += ingredients[i].price

                ingredients[i].show = true

            }
        }

        this.setState({ingredients: ingredients, total: copyTotal});

    };
    removeComponent = (name) => {
        const copyForRemove = [...this.state.ingredients];
        let copyTotal = this.state.total;

        for(let i = 0; i < copyForRemove.length; i++) {
            if(copyForRemove[i].name === name) {
                if (copyForRemove[i].count !== 0) {
                    copyForRemove[i].count--;
                    copyTotal -= copyForRemove[i].price;
                 if (copyForRemove[i].count < 1){
                        copyForRemove[i].show = false
                    }
                }
            }
        }
        this.setState({ingredients: copyForRemove, total: copyTotal})
    };


    render() {

        return (
            <div className="App container">
                <div className="ingredients">
                    <h2 className="ingredient-title">Ingredients</h2>
                    <BurgerMenu
                        onClickAdd={(name) => this.addIngredient(name)}
                        ingredients={this.state.ingredients}
                        remove={this.removeComponent}
                    />
                </div>
                <div className="hamburger">
                    <h2>Burger</h2>
                    <Burger ingredients={this.state.ingredients} total={this.state.total}/>
                </div>
            </div>
        );
    }
}

export default App;